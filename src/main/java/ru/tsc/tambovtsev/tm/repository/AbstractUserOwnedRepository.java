package ru.tsc.tambovtsev.tm.repository;

import ru.tsc.tambovtsev.tm.api.repository.IOwnerRepository;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IOwnerRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(final String userId) {
        findAll(userId).clear();
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public M findById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()) && id.equals(item.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(final String userId) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .toArray().length;
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted((Comparator<M>) sort.getComparator())
                .collect(Collectors.toList());
    }

}

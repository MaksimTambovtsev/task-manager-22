package ru.tsc.tambovtsev.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}

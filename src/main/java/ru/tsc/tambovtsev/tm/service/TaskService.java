package ru.tsc.tambovtsev.tm.service;

import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.*;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(name).orElseThrow(() -> new NameEmptyException());
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(name).orElseThrow(() -> new NameEmptyException());
        Optional.ofNullable(description).orElseThrow(() -> new DescriptionEmptyException());
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        final Task task = create(userId, name, description);
        Optional.ofNullable(task).orElseThrow(() -> new TaskNotFoundException());
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(projectId).orElseThrow(() -> new ProjectNotFoundException());
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        Optional.ofNullable(name).orElseThrow(() -> new NameEmptyException());
        final Task task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(() -> new TaskNotFoundException());
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        final Task task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(() -> new TaskNotFoundException());
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

}

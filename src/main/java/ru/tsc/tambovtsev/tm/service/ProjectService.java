package ru.tsc.tambovtsev.tm.service;


import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.repository.AbstractRepository;
import ru.tsc.tambovtsev.tm.repository.ProjectRepository;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(name).orElseThrow(() -> new NameEmptyException());
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(name).orElseThrow(() -> new NameEmptyException());
        Optional.ofNullable(description).orElseThrow(() -> new DescriptionEmptyException());
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        final Project project = create(userId, name, description);
        Optional.ofNullable(project).orElseThrow(() -> new ProjectNotFoundException());
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(name).orElseThrow(() -> new NameEmptyException());
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        final Project project = findById(userId, id);
        Optional.ofNullable(project).orElseThrow(() -> new ProjectNotFoundException());
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(() -> new UserIdEmptyException());
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        final Project project = findById(userId, id);
        Optional.ofNullable(project).orElseThrow(() -> new ProjectNotFoundException());
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

}

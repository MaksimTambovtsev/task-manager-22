package ru.tsc.tambovtsev.tm.service;

import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IProjectTaskService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.UserIdEmptyException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(() -> new IdEmptyException());
        Optional.ofNullable(taskId).orElseThrow(() -> new IdEmptyException());
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findById(taskId);
        Optional.ofNullable(task).orElseThrow(() -> new TaskNotFoundException());
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(() -> new IdEmptyException());
        Optional.ofNullable(taskId).orElseThrow(() -> new IdEmptyException());
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findById(taskId);
        Optional.ofNullable(task).orElseThrow(() -> new TaskNotFoundException());
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(() -> new IdEmptyException());
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

}

package ru.tsc.tambovtsev.tm.command.project;

import ru.tsc.tambovtsev.tm.util.TerminalUtil;

public final class ProjectCascadeRemoveCommand extends AbstractProjectCommand {

    public static final String NAME = "project-cascade-remove";

    public static final String DESCRIPTION = "Project cascade remove.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        getProjectTaskService().removeProjectById(userId, projectId);
    }

}

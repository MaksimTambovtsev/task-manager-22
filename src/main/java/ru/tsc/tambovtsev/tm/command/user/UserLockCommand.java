package ru.tsc.tambovtsev.tm.command.user;

import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public String getDescription() {
        return "User lock";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
